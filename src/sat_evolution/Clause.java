/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sat_evolution;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author trapp
 */
public class Clause {
     
    public List<Literal> literals;
    
    public Clause(List<Literal> literals){
        this.literals = literals;
    }
    
    public Clause clone(){
        List<Literal> newLiterals = new ArrayList<Literal>();
        for(Literal lit : literals )
            newLiterals.add(lit.clone());
            
        Clause cl = new Clause(newLiterals);
        return cl;
    }
    
    public List<Literal> getLiterals(){
        return literals;
    }
    
    public boolean getValue(List<Boolean> values){
        for( int i = 0; i < this.literals.size(); i++ ){
            if( ( literals.get(i).getNegation() == false ) && values.get(literals.get(i).getVariable() - 1) == true )
                return true;
            else if( ( literals.get(i).getNegation() == true ) &&  values.get(literals.get(i).getVariable() - 1) == false )
                return true;
            else
                continue;
        }        
        return false;
    }
    
    public void print(){
        System.out.print("(");
        for(int i = 0; i < this.literals.size(); i++){
            this.literals.get(i).print();
            if( (i < this.literals.size() - 1) ) 
                System.out.print(" + ");
        }
        System.out.print(")");
    }
}
