package sat_evolution;

import generator.Generator;
import java.io.FileNotFoundException;
import java.io.IOException;

/**
 *
 * @author trapp
 */
public class SAT_Evolution {

    public static void main(String[] args) throws FileNotFoundException, IOException {
        /*long overalTime = 0;
        double relative_deviation = 0;
        double worst_deviation = 0;
        int cntOfProblems = 0;
        int cntOfSolved = 0;
        int cntOfSatisfied = 0;*/
        
        if(args.length != 6 ){
            System.out.println("Wrong number of arguments, usage: ");
            System.out.println("<population_size> <number_of_generations> <elite_count> <crossover_probability> <mutation_probability> <size_of_tournament>");
            return;
        }
        
        Generator generator = new Generator(5, 22, "C:\\Users\\trapp\\Documents\\NetBeansProjects\\SAT_Evolution\\ins\\test_data_5_22.txt", 50);
        generator.generateProblem();
        
        Evolution_3SAT evol3SAT = new Evolution_3SAT("C:\\Users\\trapp\\Documents\\NetBeansProjects\\SAT_Evolution\\ins\\3sat_simple_data.txt", args);
        
        evol3SAT.solve();
        
        evol3SAT = new Evolution_3SAT("C:\\Users\\trapp\\Documents\\NetBeansProjects\\SAT_Evolution\\ins\\test_data_5_22.txt", args);
        
        evol3SAT.solve();
        
        evol3SAT = new Evolution_3SAT("C:\\Users\\trapp\\Documents\\NetBeansProjects\\SAT_Evolution\\ins\\CBS_k3_n100_m429_b30\\CBS_k3_n100_m429_b30_0.cnf", args);
        
        evol3SAT.solve();
    }
    
}
    