package sat_evolution;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

/**
 *
 * @author trapp
 */
public class Formula {
    
    public List<Boolean> values;
    private boolean value;
    private int totalWeight;
    private boolean isSatisfiable; 
    private int cntOfSatisfied;
   
    public Formula(List<Boolean> values){
        this.values = values;
        this.value = false;
        this.totalWeight = 0;
        this.isSatisfiable = false;
        this.cntOfSatisfied = 0;
    }
    
    public Formula clone(){
        List<Boolean> newValues = new ArrayList<>();
        for( int i = 0; i < this.values.size(); i++)
            newValues.add(this.values.get(i));
        Formula newForm = new Formula(newValues);
        newForm.isSatisfiable = this.isSatisfiable;
        newForm.value = this.value;
        newForm.totalWeight = this.totalWeight;
        
        return newForm;
    }
    
    public int getTotalWeight(){
        return totalWeight;
    }
    
    public int getCntOfSatisfied(){
        return cntOfSatisfied;
    }
    
    public boolean getValue(){
        return value;
    }
    
    public void getFitness(List<Integer> weights){
        this.totalWeight = 0;
        for( int i = 0; i < this.values.size(); i++ )
            if( values.get(i) )
                totalWeight += weights.get(i);
    }
    
    public void isSatisfying(List<Clause> clauses){
        this.cntOfSatisfied = 0;
        this.value = true;
        for( int i = 0; i < clauses.size(); i++ ){
            if( clauses.get(i).getValue(this.values) != true ){
                this.value = false;
            }
            else cntOfSatisfied++;
        }
    }
    
    public List<Boolean> getValues(){
        return this.values;
    }
    
    /*
        Comparator which sort an Instances by their fitness.
    */
    public static Comparator<Formula> FormulaComparator = new Comparator<Formula>() {
	public int compare(Formula f1, Formula f2) {
	   int fitness1 = f1.getTotalWeight();
	   int fitness2 = f2.getTotalWeight();
           int cntOfSatisfied1 = f1.getCntOfSatisfied();
           int cntOfSatisfied2 = f2.getCntOfSatisfied();
           boolean value1 = f1.getValue();
           boolean value2 = f2.getValue();

           /* Descending order*/
           if( value1 && value2 )
               return fitness2 - fitness1;
           else if( value1 && !value2 )
               return -1;
           else if( !value1 && value2 )
               return 1;
           else{
               if( cntOfSatisfied1 == cntOfSatisfied2 )
                   return fitness2 - fitness1;
               else if( cntOfSatisfied1 > cntOfSatisfied2 )
                   return -1;
               else
                   return 1;
               
           }
    }};
    
}
