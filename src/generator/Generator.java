package generator;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Random;

public class Generator {
    private int variables;
    private int clauses;
    private int instances;
    private File file;
    private FileWriter fr;
    private BufferedWriter br;
    Random rand;
    
    
    public Generator(int variables, int clauses, String fileName, int instances){
        this.variables = variables;
        this.clauses = clauses;
        this.instances = instances;
        this.rand = new Random(); 
        file = new File(fileName);
    }
    
    public void generateProblem(){
            try{
                fr = new FileWriter(file);
                br = new BufferedWriter(fr);
                for(int k = 0; k < this.instances; k++){
                    String data = "p cnf " + variables + " " + clauses + System.getProperty("line.separator");
                    br.write(data);
                    int valVariable, valNegation, i = 0;
                    boolean flag = false;
                    while(i < clauses){
                        data = "";
                        flag = false;
                        for(int j = 1; j < variables + 1; j++){
                            valVariable = rand.nextInt(6) + 1;
                            if( valVariable > 2 ){
                                flag = true;
                                valNegation = rand.nextInt(2);
                                if( valNegation > 0 )
                                    data += j + " ";
                                else
                                    data += "-" + j + " ";
                            }
                        }
                        if( flag == false )
                            continue;
                        else{
                            data += "0" + System.getProperty("line.separator");
                            br.write(data);
                            i++;
                        }
                    }
                    data = "0" + System.getProperty("line.separator");
                    br.write(data);
                }
            } catch (IOException e) {
                e.printStackTrace();
            }finally{
                try {
                    br.close();
                    fr.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
}
